package main

import (
	"ltkgo"
	"log"
	"fmt"
	"time"
	"flag"
	"os"
	"runtime/pprof"
	"runtime"
)

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var testTime = flag.Int("testtime", 10, "Length for test to run in seconds")
var reader = flag.String("reader", "", "LLRP Reader to connect to")
var fullTransaction = flag.Bool("full", false, "Send a full LLRP connect/RO sequence")
var jsonMarshal = flag.Bool("json", false, "Marshal reports to json instead of standard MM")
var defPath = flag.String("defs", "", "LLRP XML Definitions to load")

func main() {
	flag.Parse()

	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	if *reader == "" {
		fmt.Println("Must specify a reader with the -reader flag!")
		return
	}

	if *defPath == "" {
		fmt.Println("Must specify LLRP Definitions to load with the -defs flag!")
		return
	}

	client := ltkgo.NewLLRPClient()

	err := client.LoadXmlDefs(*defPath)

	if err != nil {
		log.Fatal(err)
		return
	}

	err = client.Connect(*reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Only issue the full transaction if requested by the user,
	// otherwise just sit and listen for tag reports
	if *fullTransaction {
		add_ro := []byte(`
                    {
                      "ADD_ROSPEC":
                      {
                        "ROSpec":
                        {
                          "ROSpecID": 1,
                          "CurrentState":"Disabled",
                          "AISpec":
                          {
                            "InventoryParameterSpec":
                            {
                              "InventoryParameterSpecID": 1,
                              "ProtocolID": "EPCGlobalClass1Gen2"
                            }
                          }
                        }
                      }
                    }`)



		_, err  = client.TransactJson(add_ro, false)
		if err != nil {
			log.Fatal(err)
			return
		}

		en_ro := []byte (`
                         {
                           "ENABLE_ROSPEC":
                           {
                             "ROSpecID": 1
                           }
                         }
                         `)

		_, err  = client.TransactJson(en_ro, false)
		if err != nil {
			log.Fatal(err)
			return
		}

		sr_ro := []byte (`
                         {
                           "START_ROSPEC":
                           {
                             "ROSpecID": 1
                           }
                         }
                         `)
		_, err  = client.TransactJson(sr_ro, false)
		if err != nil {
			log.Fatal(err)
			return
		}
	}

	start := time.Now()
	tagCount := 0


	for time.Since(start) < time.Duration(*testTime * int(time.Second)) {
		var cnt int
		if *jsonMarshal {
			_, cnt = client.CollectJson(1)
		} else {
			_, cnt = client.CollectMM(1)
		}
		tagCount += cnt
	}

	if *fullTransaction {

		sp_ro := []byte (`
                         {
                           "STOP_ROSPEC":
                           {
                             "ROSpecID": 1
                           }
                         }
                         `)
		_, err  = client.TransactJson(sp_ro, false)
		if err != nil {
			log.Fatal(err)
			return
		}
		runtime.Gosched()
	}

	client.Disconnect()

	fmt.Printf("Decoded %d tags\n", tagCount)
	fmt.Printf("%d tags/second\n", tagCount / *testTime)
}
