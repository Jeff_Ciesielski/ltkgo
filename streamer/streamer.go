package main

import (
	"net"
	"fmt"
	"time"
	"flag"
)

var streamRate = flag.Float64("streamrate", 1600, "Number of tags/second to stream")

func tagStreamer(conn net.Conn, delay float64) {
	tagreport := []byte{4, 61, 0, 0, 0, 41, 65, 29, 9, 237, 0, 240,
		0, 31, 141, 48, 8, 51, 178, 221, 217, 1, 64, 0, 0, 0, 0,
		129, 0, 1, 134, 217, 130, 0, 5, 13, 234, 110, 110, 87, 244}

	for {
		tStart := time.Now()
		for time.Since(tStart) < time.Duration(delay * float64(time.Microsecond)) {
		}

		_, err := conn.Write(tagreport)
		if err != nil {
			fmt.Printf("Connection from: %s was terminated\n", conn.RemoteAddr())
			return
		}
	}
}

func main() {
	flag.Parse()

	iFaces, err := net.InterfaceAddrs()

	ln, err := net.Listen("tcp", ":5084")
	if err != nil {
		fmt.Println("Unable to open LLRP listening socket")
		return
	}

	delay := 1000000 / *streamRate

	fmt.Println("Listening for LLRP connections @")
	for i := range iFaces {
		ifName := iFaces[i].String()
		if ifName[0] != ':' {
			fmt.Println("-", ifName)
		}
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			fmt.Println("Unable to accept connection")
			return
		}
		fmt.Printf("Accepted connection from: %s\n", conn.RemoteAddr())
		go tagStreamer(conn, delay)
	}
}
