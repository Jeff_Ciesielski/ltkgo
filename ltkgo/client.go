package ltkgo

import (
	"net"
	"fmt"
	"errors"
	"sync"
	"bytes"
	"strings"
	"container/list"
	"runtime"
	"io"
)

type LLRPClient struct {
	sync.RWMutex
	defStore *LLRPDefinitionStore
	conn net.Conn
	connected bool
	msg_id uint32
	respChan chan *MsgMember
	reports *list.List
	stopChan chan bool
	Address string
}

func readU16(b *bytes.Buffer) (u uint16) {
	u1, _ := b.ReadByte()
	u2, _ := b.ReadByte()
	u = uint16((uint16(u1) << 8) | uint16(u2))
	return u
}

func readU32(b *bytes.Buffer) (u uint32) {
	u1, _ := b.ReadByte()
	u2, _ := b.ReadByte()
	u3, _ := b.ReadByte()
	u4, _ := b.ReadByte()
	u = uint32((uint32(u1) << 24) | (uint32(u2) << 16) | (uint32(u3) << 8) | uint32(u4))
	return u
}

func (cli *LLRPClient) listen() {

	fmt.Println("Listening...")
	for {
		select {
		case _ = <- cli.stopChan:
			fmt.Println("Stopping listen routine")
			cli.conn.Close()
			return
		default:
			// Pass
		}

		bl := new(bytes.Buffer)
		msgHeader := new(bytes.Buffer)

		// Temporary buffer to hold the message header
		tmp := make([]byte, 10)
		_, err := cli.conn.Read(tmp)
		if err != nil {
			fmt.Println("Error reading header")
			// TODO: how to handle gracefully
			return
		}

		// First off, append the message header to the buffer
		bl.Write(tmp)

		// Now, convert the message header to a temporary byte
		// buffer so we can strip out the values we need
		msgHeader.Write(tmp)

		// Grab the u16 representing the message type and llrp
		// version from the header
		var tempU16 uint16 = readU16(msgHeader)

		// Strip out the type and version
		msgType := tempU16 & 0x3ff
		msgVer := (tempU16 & 0x1C00) >> 10

		//fmt.Println("Receiving message type:", msgType)

		if msgVer != 1 {
			//fmt.Println("Unexpected message version:", msgVer)
			continue
		}

		if _, ok := cli.defStore.MessageIntMap[int(msgType)]; !ok {
			//fmt.Println("Unknown message type:", msgType)
			continue
		}

		// Now grab the message length
		var msgLen uint32 = readU32(msgHeader)

		// Check to ensure that the message length makes sense
		if msgLen < 10 || msgLen > 1024 * 4 {
			//fmt.Println("Bad message length")
			continue
		}

		// Discard the message ID
		_ = readU32(msgHeader)

		//fmt.Println(msgLen)
		// Now we know how much we need to read
		// Make a buffer with len == msgLen - 10
		payloadBuf := make([]byte, msgLen - 10)

		// Now attempt to fill the payload buffer
		_, err = cli.conn.Read(payloadBuf)
		if err != nil {
			fmt.Println("Unable to receive payload buffer", err)
			if err == io.EOF {
				continue
			}
			//fmt.Println(msgID)
			// TODO: how to handle gracefully
		}

		// Now append the payload to the byte buffer
		bl.Write(payloadBuf)

		fmt.Println("rxBytes:", bl.Bytes())

		// Attempt to decode
		mm, err := cli.defStore.decodeMsg(bl)

		if err != nil {
			fmt.Println("Error decoding message", err)
			continue
		}

		if strings.Contains(mm.Key, "RESPONSE") {
			fmt.Println("Got:", mm.Key)
			cli.respChan <- mm
		} else {
			//fmt.Println("rep")
			cli.Lock()
			cli.reports.PushBack(mm)
			cli.Unlock()
		}

	}
}

func (cli *LLRPClient) Connect(addr string) error {
	var err error
	cli.conn, err = net.Dial("tcp", addr+":5084")
	if err != nil {
		fmt.Println("error connecting")
		return err
	}
	fmt.Println("Successfully connected to:", addr)
	cli.connected = true
	cli.Address = addr
	go cli.listen()

	return nil
}

func (cli *LLRPClient) Disconnect() error {
	cli.stopChan <- true
	return nil
}


func (cli *LLRPClient) getMsg() (mm *MsgMember) {
	e := cli.reports.Front()
	mm = e.Value.(*MsgMember)

	cli.Lock()
	cli.reports.Remove(e)
	cli.Unlock()
	return mm
}

func (cli *LLRPClient) GetMsgBufCount() int {
	return cli.reports.Len()
}

func (cli *LLRPClient) CollectMM(num uint32) (retList *list.List, retCount int) {
	retList = list.New()
	retCount = 0
	for i := uint32(0); i < num; i++ {
		if cli.reports.Len() > 0 {
			mm := cli.getMsg()
			retList.PushBack(mm)
			retCount++
		} else {
			// Ensures the listen thread gets even time
			runtime.Gosched()
			break
		}
	}
	return retList, retCount
}

func (cli *LLRPClient) CollectJson(num uint32) (retList *list.List, retCount int ) {
	retList = list.New()
	retCount = 0
	for i := uint32(0); i < num; i++ {
		if cli.reports.Len() > 0 {
			mm := cli.getMsg()
			jsonstr, err := mm.toJson(true)
			if err == nil {
				retList.PushBack(jsonstr)
				retCount++
			}
		} else {
			runtime.Gosched()
			break
		}

	}
	return retList, retCount

}

func (cli *LLRPClient) TransactJson(jsn []byte, strict bool) ([]byte, error) {
	if cli.connected != true {
		return nil, errors.New("LLRP Client is not connected to a reader")
	}

	mm, err := MsgMemberFromJson(jsn)
	if err != nil {
		return nil, err
	}

	cli.Lock()
	txBytes, err := cli.defStore.encodeMsg(mm, cli.msg_id, strict)
	cli.msg_id++
	cli.Unlock()
	if err != nil {
		return nil, err
	}

	fmt.Println("txBytes:", txBytes)

	n, err := cli.conn.Write(*txBytes)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Wrote %d bytes\n", n)

	retmm := <-cli.respChan

	fmt.Println("Received JSON:")
	jsonstr, err := retmm.toJson(true)
	fmt.Println(string(*jsonstr))

	return *jsonstr, nil
}

func NewLLRPClient() (*LLRPClient) {
	cli := new(LLRPClient)
	cli.defStore = NewLLRPDefinitionStore()
	cli.respChan = make(chan *MsgMember)
	cli.reports = list.New()
	cli.stopChan = make(chan bool)
	return cli
}

func (cli *LLRPClient) LoadXmlDefs(filePath string) (error) {
	err := cli.defStore.ImportXmlDef(filePath)
	cli.msg_id = 0
	return err
}
