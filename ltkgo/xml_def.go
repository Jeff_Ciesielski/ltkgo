package ltkgo

import (
	"encoding/xml"
	"os"
	"strconv"
	"math"
	"strings"
	"errors"
)
// TODO: Handle Namespaces...
// TODO: Rethink choice mapping/ChoiceDef vs ChoiceMember vs Choice
func (lds *LLRPDefinitionStore) populateChoiceDefinition(startElement xml.StartElement) error {

	choiceDef := new(LLRPChoiceDef)

	// Initialize the ChoiceMap
	choiceDef.ChoiceMap = make(map[string]*LLRPChoiceMember)

	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "name":
			choiceDef.Name = curAttr.Value
			lds.ChoiceNameMap[curAttr.Value] = choiceDef
		}
	}
	return nil
}

func (lds *LLRPDefinitionStore) populateMessageDefinition(startElement xml.StartElement) error {

	msgDef := new(LLRPMessageDef)

	// Initialize the members list in the message definition
	msgDef.Members.Init()

	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "name":
			msgDef.Name = curAttr.Value
			lds.MessageNameMap[curAttr.Value] = msgDef
		case "typeNum":
			typeNum, err := strconv.Atoi(curAttr.Value)
			if err != nil {
				return err
			}
			msgDef.TypeNum = typeNum
			lds.MessageIntMap[typeNum] = msgDef
		case "Required":
			if curAttr.Value == "true" {
				msgDef.Required = true
			} else {
				msgDef.Required = false
			}
		case "responseType":
			msgDef.ResponseTypeName = curAttr.Value
		}
	}
	return nil
}

func (lds *LLRPDefinitionStore) populateParameterDefinition(startElement xml.StartElement) error {

	paramDef := new(LLRPParameterDef)

	paramDef.Encoding = TV
	// Initialize the param def members list
	paramDef.Members.Init()

	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "name":
			paramDef.Name = curAttr.Value
			lds.ParamNameMap[curAttr.Value] = paramDef
		case "typeNum":
			typeNum, err := strconv.Atoi(curAttr.Value)
			if err != nil {
				return err
			}
			paramDef.TypeNum = typeNum
			lds.ParamIntMap[typeNum] = paramDef
		case "Required":
			if curAttr.Value == "true" {
				paramDef.Required = true
			} else {
				paramDef.Required = false
			}
		}
	}

	return nil
}

func (lds *LLRPDefinitionStore) populateEnumerationDefinition(startElement xml.StartElement) error {

	enumDef := new(LLRPEnumDef)

	// We need to make sure that the value map is initialized,
	// this is as good a place as any
	enumDef.ValMap = make(map[string]int)

	for k := range startElement.Attr {
			curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "name":
			enumDef.Name = curAttr.Value
			lds.EnumNameMap[curAttr.Value] = enumDef
		}
	}

	return nil
}

func (lds *LLRPDefinitionStore) populateDefMaps(xmlFile *os.File) error {

	xmlFile.Seek(0,0)

	xmlDecoder := xml.NewDecoder(xmlFile)

	for {
		token, _ := xmlDecoder.Token()
		if token == nil {
			break
		}
		switch element := token.(type) {
		case xml.StartElement:
			err := lds.populateElement(element)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (lds *LLRPDefinitionStore) newFieldFromElement (startElement xml.StartElement) (*LLRPField, error) {
	field := new(LLRPField)

	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "name":
			field.Name = curAttr.Value
		case "type":
			field.Type = curAttr.Value
		case "enumeration":
			field.Enumeration = lds.EnumNameMap[curAttr.Value]
		}
	}
	return field, nil
}

func (lds *LLRPDefinitionStore) newParameterFromElement (startElement xml.StartElement) (*LLRPParameter, error) {
	param := new(LLRPParameter)

	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "repeat":
			s := strings.Split(curAttr.Value, "-")
			min, err := strconv.Atoi(s[0])
			if err != nil {
				return nil, err
			}

			param.RepeatMin = min

			if len(s) > 1 {
				if s[1] == "N" {
					param.RepeatMax = math.MaxInt32
				} else {
					max , err := strconv.Atoi(s[1])
					if err != nil {
						return nil, err
					}
					param.RepeatMax = max

				}
			} else {
				param.RepeatMax = param.RepeatMin
			}

		case "type":
			param.Type = lds.ParamNameMap[curAttr.Value]
		}
	}
	return param, nil
}

func (lds *LLRPDefinitionStore) addMemberToMessageDef(lmd *LLRPMessageDef, startElement xml.StartElement) error {
	switch startElement.Name.Local {
	case "field":
		field, err := lds.newFieldFromElement(startElement)
		if err != nil {
			return err
		}
		lmd.Members.PushBack(field)
	case "parameter":
		param, err := lds.newParameterFromElement(startElement)
		if err != nil {
			return err
		}
		lmd.Members.PushBack(param)
	}

	return nil
}

func (lds *LLRPDefinitionStore) addParamsToChoiceDef(lcd *LLRPChoiceDef, startElement xml.StartElement) error {
	switch startElement.Name.Local {
	case "parameter":
		// Get a pointer to the Parameter definition that we
		// want to add to this choice definition
		for k := range startElement.Attr {
			curAttr := startElement.Attr[k]
			elName := curAttr.Name.Local
			if elName == "type" {
				choice := new(LLRPChoiceMember)
				var name string

				// Ideally, a choice will contain one
				// or more parameters, but
				// occasionally, one of these
				// parameters will be another
				// choice... We need to account for this
				paramDef := lds.ParamNameMap[curAttr.Value]
				if paramDef == nil {
					choiceDef := lds.ChoiceNameMap[curAttr.Value]
					if choiceDef == nil {
						return errors.New("Bad choice definition")
					} else {
						choice.Choice = choiceDef
						name = choiceDef.Name
					}
				} else {
					choice.Param = paramDef
					name = paramDef.Name
				}
				lcd.ChoiceMap[name] = choice
				break
			}
		}
	}

	return nil
}

func (lds *LLRPDefinitionStore) mapChoiceDefinition(startElement xml.StartElement, decoder *xml.Decoder) error {

	var choiceDef *LLRPChoiceDef

	// Get a pointer to the Choice definition that we want to
	// map out
	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		if elName == "name" {
			choiceDef = lds.ChoiceNameMap[curAttr.Value]
			break
		}
	}

	// Now, walk through every token underneath this startElement
	// and fill out fields accordingly.  When we find the
	// EndElement that matches the StartElement that was passed
	// into this function, we can return

	// Flag to know when to halt this loop
	var elemClosed = false
	for elemClosed == false {
		token, _ := decoder.Token()
		if token == nil {
			break
		}
		switch element := token.(type) {
		case xml.StartElement:
			err := lds.addParamsToChoiceDef(choiceDef, element)
			if err != nil {
				return err
			}
		case xml.EndElement:
			// If this is the end of the message
			// definition, mark that the element is closed
			// which will cause us to bail out of this for loop
			if element.Name.Local == startElement.Name.Local {
				elemClosed = true
			}
		}
	}
	return nil
}

func (lds *LLRPDefinitionStore) mapMessageDefinition(startElement xml.StartElement, decoder *xml.Decoder) error {

	var msgDef *LLRPMessageDef

	// Get a pointer to the message definition that we want to
	// map out
	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		if elName == "name" {
			msgDef = lds.MessageNameMap[curAttr.Value]
			break
		}
	}

	// Now, walk through every token underneath this startElement
	// and fill out fields accordingly.  When we find the
	// EndElement that matches the StartElement that was passed
	// into this function, we can return

	// Flag to know when to halt this loop
	var elemClosed = false
	for elemClosed == false {
		token, _ := decoder.Token()
		if token == nil {
			break
		}
		switch element := token.(type) {
		case xml.StartElement:
			err := lds.addMemberToMessageDef(msgDef, element)
			if err != nil {
				return err
			}
		case xml.EndElement:
			// If this is the end of the message
			// definition, mark that the element is closed
			// which will cause us to bail out of this for loop
			if element.Name.Local == startElement.Name.Local {
				elemClosed = true
			}
		}
	}
	return nil
}

func (lds *LLRPDefinitionStore) newReservedFromElement (startElement xml.StartElement) (*LLRPReserved, error) {
	res := new(LLRPReserved)

	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "bitCount":
			bits, err := strconv.Atoi(curAttr.Value)
			if err != nil {
				return nil, err
			}
			res.BitCount = bits
		}
	}
	return res, nil
}

func (lds *LLRPDefinitionStore) newChoiceFromElement (startElement xml.StartElement) (*LLRPChoice, error) {
	choice := new(LLRPChoice)
	
	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		switch elName {
		case "repeat":
			s := strings.Split(curAttr.Value, "-")
			min, err := strconv.Atoi(s[0])
			if err != nil {
				return nil, err
			}

			choice.RepeatMin = min

			if len(s) > 1 {
				if s[1] == "N" {
					choice.RepeatMax = math.MaxInt32
				} else {
					max , err := strconv.Atoi(s[1])
					if err != nil {
						return nil, err
					}
					choice.RepeatMax = max

				}
			} else {
				choice.RepeatMax = choice.RepeatMin
			}

		case "type":
			choice.Type = lds.ChoiceNameMap[curAttr.Value]
		}
	}
	return choice, nil
}

func (lds *LLRPDefinitionStore) addMemberToParamDef(lpd *LLRPParameterDef, startElement xml.StartElement) error {
	switch startElement.Name.Local {
	case "field":
		field, err := lds.newFieldFromElement(startElement)
		if err != nil {
			return err
		}
		lpd.Members.PushBack(field)
	case "reserved":
		res, err := lds.newReservedFromElement(startElement)
		if err != nil {
			return err
		}
		lpd.Members.PushBack(res)
	case "parameter":
		lpd.Encoding = TLV
		param, err := lds.newParameterFromElement(startElement)
		if err != nil {
			return err
		}
		lpd.Members.PushBack(param)
	case "choice":
		lpd.Encoding = TLV
		choice, err := lds.newChoiceFromElement(startElement)
		if err != nil {
			return err
		}
		lpd.Members.PushBack(choice)
	}

	return nil
}

func (lds *LLRPDefinitionStore) mapParameterDefinition(startElement xml.StartElement, decoder *xml.Decoder) error {
	var paramDef *LLRPParameterDef

	// Get a pointer to the message definition that we want to
	// map out
	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		if elName == "name" {
			paramDef = lds.ParamNameMap[curAttr.Value]
			break
		}
	}

	// Now, walk through every token underneath this startElement
	// and fill out fields accordingly.  When we find the
	// EndElement that matches the StartElement that was passed
	// into this function, we can return

	// Flag to know when to halt this loop
	var elemClosed = false
	for elemClosed == false {
		token, _ := decoder.Token()
		if token == nil {
			break
		}
		switch element := token.(type) {
		case xml.StartElement:
			err := lds.addMemberToParamDef(paramDef, element)
			if err != nil {
				return err
			}
		case xml.EndElement:
			// If this is the end of the message
			// definition, mark that the element is closed
			// which will cause us to bail out of this for loop
			if element.Name.Local == startElement.Name.Local {
				elemClosed = true
			}
		}
	}
	return nil
}

func (lds *LLRPDefinitionStore) addMemberToEnumDef(led *LLRPEnumDef, startElement xml.StartElement) error {
	var key string
	var val int
	var err error

	switch startElement.Name.Local {
	case "entry":
		// Pull out the key/value pairs from the enumeration entry
		for k := range startElement.Attr {
			curAttr := startElement.Attr[k]
			elName := curAttr.Name.Local
			switch elName {
			case "value":
				val, err = strconv.Atoi(curAttr.Value)
				if err != nil {
					return err
				}
			case "name":
				key = curAttr.Value
			}
		}
		led.ValMap[key] = val
	}

	return nil
}

func (lds *LLRPDefinitionStore) mapEnumDefinition(startElement xml.StartElement, decoder *xml.Decoder) error {
	var enumDef *LLRPEnumDef

	// Get a pointer to the message definition that we want to
	// map out
	for k := range startElement.Attr {
		curAttr := startElement.Attr[k]
		elName := curAttr.Name.Local
		if elName == "name" {
			enumDef = lds.EnumNameMap[curAttr.Value]
			break
		}
	}

	// Now, walk through every token underneath this startElement
	// and fill out fields accordingly.  When we find the
	// EndElement that matches the StartElement that was passed
	// into this function, we can return

	// Flag to know when to halt this loop
	var elemClosed = false
	for elemClosed == false {
		token, _ := decoder.Token()
		if token == nil {
			break
		}
		switch element := token.(type) {
		case xml.StartElement:
			err := lds.addMemberToEnumDef(enumDef, element)
			if err != nil {
				return err
			}
		case xml.EndElement:
			// If this is the end of the message
			// definition, mark that the element is closed
			// which will cause us to bail out of this for loop
			if element.Name.Local == startElement.Name.Local {
				elemClosed = true
			}
		}
	}

	return nil
}

func (lds *LLRPDefinitionStore) mapElement(element xml.StartElement, decoder *xml.Decoder) error {
	switch element.Name.Local {
	case "messageDefinition":
		lds.mapMessageDefinition(element, decoder)
	case "parameterDefinition":
		lds.mapParameterDefinition(element, decoder)
	case "enumerationDefinition":
		lds.mapEnumDefinition(element, decoder)
	case "choiceDefinition":
		lds.mapChoiceDefinition(element, decoder)
	}
	return nil
}

func (lds *LLRPDefinitionStore) buildDefHeirarchy(xmlFile *os.File) error {
	xmlFile.Seek(0,0)

	xmlDecoder := xml.NewDecoder(xmlFile)

	for {
		token, _ := xmlDecoder.Token()
		if token == nil {
			break
		}
		switch element := token.(type) {
		case xml.StartElement:
			err := lds.mapElement(element, xmlDecoder)
			if err != nil {
				return err
			}
		}
	}

	return nil
}

func (lds *LLRPDefinitionStore) populateElement(startElement xml.StartElement) error {

	switch startElement.Name.Local {
	case "messageDefinition":
		lds.populateMessageDefinition(startElement)
	case "parameterDefinition":
		lds.populateParameterDefinition(startElement)
	case "enumerationDefinition":
		lds.populateEnumerationDefinition(startElement)
	case "choiceDefinition":
		lds.populateChoiceDefinition(startElement)
	}
	return nil
}

func (lds *LLRPDefinitionStore) ImportXmlDef(filePath string) error {
	// Open the xml file
	xmlFile, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer xmlFile.Close()

	err = lds.populateDefMaps(xmlFile)
	if err != nil {
		return err
	}
	lds.buildDefHeirarchy(xmlFile)
	if err != nil {
		return err
	}

	return nil
}
