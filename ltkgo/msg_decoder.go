package ltkgo

import (
	"fmt"
	"bytes"
	//"encoding/hex"
)

func(lds *LLRPDefinitionStore) decodeField(lf *LLRPField, mm *MsgMember, bl *byteList) error {
	//fmt.Println("Decoding Field:", lf.Name, bl.getBytes())
	switch lf.Type {
	case "u96":
		bytes, err := bl.getByteSlice(12)
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(bytes)
		mm.AddValue(&newMM)
	case "u1":
		v, err := bl.getU8()
		if err != nil {
			return err
		}
		v = uint8(v >> 7)
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(v))
		mm.AddValue(&newMM)
	case "u1v":
		vLen, err := bl.getU16()
		if err != nil {
			return err
		}

		// Calculate the length we need to grab, if this
		// number isn't evenly divisible, round up
		calcLen := int(vLen / 8)
		if vLen % 8 != 0 {
			calcLen++
		}

		// Grab the byte slice
		bytes, err := bl.getByteSlice(calcLen)
		if err != nil {
			return err
		}

		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(bytes)
		mm.AddValue(&newMM)
	case "u2":
		v, err := bl.getU8()
		if err != nil {
			return err
		}
		v = uint8(v >> 6)
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(v))
		mm.AddValue(&newMM)
	case "u8":
		v, err := bl.getU8()
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(v))
		mm.AddValue(&newMM)
	case "s8":
		v, err := bl.getU8()
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(int8(v)))
		mm.AddValue(&newMM)
	case "u8v":
		vLen, err := bl.getU16()
		if err != nil {
			return err
		}

		// Calculate the length we need to grab, if this
		// number isn't evenly divisible, round up

		// Grab the byte slice
		bytes, err := bl.getByteSlice(int(vLen))
		if err != nil {
			return err
		}

		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(bytes)
		mm.AddValue(&newMM)
	case "utf8v":
		// We treat this like a string
		vLen, err := bl.getU16()
			if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		strBytes := new(bytes.Buffer)
		for i := 0; i < int(vLen); i++ {
			v, err := bl.getU8()
			if err != nil {
				return err
			}
			strBytes.WriteByte(byte(v))
		}
		newMM.AddValue(strBytes.String())
		mm.AddValue(&newMM)
	case "u16":
		v, err := bl.getU16()
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(v))
		mm.AddValue(&newMM)
	case "u16v":
		vLen, err := bl.getU16()
		if err != nil {
			return err
		}

		newMM := MsgMember{Key: lf.Name}
		fSlice := make([]float64, vLen)
		for i := 0; i < int(vLen); i++ {
			v, err := bl.getU16()
			if err != nil {
				return err
			}
			fSlice[i] = float64(v & 0xffff)
		}
		newMM.AddValue(&fSlice)
		mm.AddValue(&newMM)
	case "u32":
		v, err := bl.getU32()
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(v))
		mm.AddValue(&newMM)
	case "u32v":
		vLen, err := bl.getU16()
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		fSlice := make([]float64, vLen)
		for i := 0; i < int(vLen); i++ {
			v, err := bl.getU32()
			if err != nil {
				return err
			}
			fSlice[i] = float64(v & 0xffffffff)
		}
		newMM.AddValue(&fSlice)
		mm.AddValue(&newMM)
	case "u64":
		//fmt.Println("u64 field")
		v, err := bl.getU64()
		if err != nil {
			return err
		}
		newMM := MsgMember{Key: lf.Name}
		newMM.AddValue(float64(v))
		mm.AddValue(&newMM)
	}
	return nil
}

func(lds *LLRPDefinitionStore) getNextParamType(bl *byteList) (int, uint8, error) {


	// Pre check to ensure we can even perform these lookups, this
	// will allow for skipping of error checking
	if bl.Len() < 2 {
		return 0, 0, fmt.Errorf("Not enough bytes to compose a parameter")
	}

	// Check to see if this parameter is a TV or TLV parameter
	encodingByte, _ := bl.peekU8()

	var paramType uint16
	var encoding uint8 = TLV

	if (encodingByte & (1 << 7)) >> 7 == 1 {
		encoding = TV
	}

	if encoding == TLV {
		//fmt.Println("TLV")
		// Get the parameter type
		p, _ := bl.peekU16()
		paramType = p
	} else {
		//fmt.Println("TV")
		p, _ := bl.peekU8()
		paramType = uint16(p & 0x7f)
	}
	return  int(paramType), encoding, nil
}

func(lds *LLRPDefinitionStore) decodeParam(paramType int, encoding uint8, lp *LLRPParameter, mm *MsgMember, bl *byteList) error {
	//fmt.Println("decodeParam")

	//fmt.Println(bl.getBytes())


	var err error

	//fmt.Println("Got param type:", paramType)
	//fmt.Println("Expecting Param type:", lp.Type.TypeNum)

	// TODO: Verify we still need this check
	// Make sure the parameter is of the type we're expecting to see
	if paramType != lp.Type.TypeNum {
		if lp.RepeatMin > 0 {
			// If we were supposed to get this parameter
			// (minimum count > 0), throw an error
			//fmt.Println("Missing required parameter", lp.Type.Name, lp.Type.TypeNum)
			//fmt.Println(bl.getBytes())

			return fmt.Errorf("Missing required parameter")
		}
		//fmt.Println("Unnecessary parameter")
		// Otherwise return nil
		return nil;
	}

	if encoding == TLV {
		// Discard the parameter type and parameter length
		_, err = bl.getU32()
		if err != nil {
			return err
		}
	} else {
		_, err = bl.getU8()
		if err != nil {
			return err
		}
	}

	pmm := NewMsgMember()

	pmm.Key = lp.Type.Name

	//fmt.Println("Decoding Param:", lp.Type.Name)

	// Perform some lookahead to ensure we don't perform
	// unnecessary actions
	nextParam, encoding, err := lds.getNextParamType(bl)
	if err != nil {
		member := lp.Type.Members.Front()
		switch member.Value.(type) {
		case *LLRPParameter:
			param := member.Value.(*LLRPParameter)
			fmt.Println("Failed GetParamType looking for param:", param.Type.Name)
			return err
		case *LLRPChoice:
			fmt.Println("Failed GetParamType looking for choice")
			return err
		case *LLRPReserved:
			fmt.Println("Failed GetParamType looking for reserved")
			return err
		case *LLRPField:
			field := member.Value.(*LLRPField)
			fmt.Println("Failed GetParamType looking for Field:", field.Name)
			break
		}
	}

	// Walk through each parameter member and decode it.  If we
	// run out of bytes in our bytelist, we've come to the end of
	// the message and should abort
	for member := lp.Type.Members.Front(); member != nil && bl.Len() > 0; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			param := member.Value.(*LLRPParameter)

			if param.Type.TypeNum == nextParam {
				err = lds.decodeParam(nextParam, encoding, param, pmm, bl)
				if err != nil {
					return err
				}
				nextParam, encoding, _ = lds.getNextParamType(bl)
				// TODO: Should we handle this error?
			} else if param.RepeatMin > 0 {
				return fmt.Errorf("Missing required parameter:%s\n", lp.Type.Name)
			}

		case *LLRPChoice:
			choice := member.Value.(*LLRPChoice)
			//choice.Print(1)
			// Iterate over the choice parameter map

			//fmt.Println("Trying:", nextParam)
			np, ok := lds.ParamIntMap[nextParam]
			if !ok && choice.RepeatMin > 0 {
				return fmt.Errorf("Missing required choice")
			} else if !ok {
				continue
			}

			if paramDef, ok := choice.Type.ChoiceMap[np.Name]; ok {
				param := new(LLRPParameter)
				param.RepeatMin = choice.RepeatMin
				param.RepeatMax = choice.RepeatMax
				param.Type = paramDef.Param
				err = lds.decodeParam(nextParam, encoding, param, pmm, bl)
				if err != nil {
					return err
				}
				nextParam, encoding, err = lds.getNextParamType(bl)
				if err != nil {
					return err
				}
			}

		case *LLRPField:
			field := member.Value.(*LLRPField)
			err := lds.decodeField(field, pmm, bl)
			if err != nil {
				return err;
			}
		case *LLRPReserved:
			// No op
		}
	}

	mm.AddValue(pmm)

	return nil
}

func(lds *LLRPDefinitionStore) decodeMsg(b *bytes.Buffer) (mm *MsgMember, err error) {

	bList := newByteListFromBuf(b)
	mm = NewMsgMember()

	//fmt.Println(bList.getBytes())
	// Get the Message Key
	tHeader, err := bList.getU16()
	if err != nil {
		//fmt.Println("Unable to get message key")
		return nil, err
	}

	mVer := (tHeader & (0x1 << 10)) >> 10
	mType := int(tHeader & 0x3ff)
	if mVer != 0x01 {
		return nil, fmt.Errorf("Invalid message version: %d", mVer)
	}

	msgDef, ok := lds.MessageIntMap[mType]
	if !ok {
		return nil, fmt.Errorf("Unknown Message Type: %d\n", mType)
	}

	// Discard the message length for now
	_, err = bList.getU32()
	if err != nil {
		return nil, err
	}

	// Discard the message ID for now
	_, err = bList.getU32()
	if err != nil {
		return nil, err
	}

	mm.Key = msgDef.Name

	//fmt.Println("Decoding message:", mm.Key)

	// TODO: Handle custom messages

	for member := msgDef.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			lp := member.Value.(*LLRPParameter)
			// Perform some lookahead to avoid unnecessary
			// decoding attempts
			nextParam, encoding, err := lds.getNextParamType(bList)
			if err != nil  && lp.RepeatMin > 0 {
				return nil, err
			}
			if lp.Type.TypeNum == nextParam {
				err = lds.decodeParam(nextParam, encoding, lp, mm, bList)
				if err != nil {
					return nil, err
				}
			} else if lp.RepeatMin > 0 {
				return nil, fmt.Errorf("Missing required parameter:%s\n", lp.Type.Name)
			}
		case *LLRPField:
			lf := member.Value.(*LLRPField)
			err = lds.decodeField(lf, mm, bList)
			if err != nil {
				return nil, err
			}
		}
	}


	return mm, err
}
