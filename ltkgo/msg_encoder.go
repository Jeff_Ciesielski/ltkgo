package ltkgo

import (
	"fmt"
	"container/list"
)

func (lf *LLRPField) getFieldValue(fVal *list.Element) (float64, error) {
	rawVal := 0.0
	fmt.Println("Getting field value")
	switch fVal.Value.(type) {
	case float64:
		// Just a number
		fmt.Println("Trying f64")
		rawVal = fVal.Value.(float64)
	case string:
		// Enumeration string, we need to look this up and
		// make sure it exists
		tVal, ok := lf.Enumeration.ValMap[fVal.Value.(string)]
		if !ok {
			return 0, fmt.Errorf("Invalid enumeration value %s in field: %s",
				fVal.Value.(string), lf.Name)
		}
		rawVal = float64(tVal)
	case *MsgMember:
		return 0, fmt.Errorf("Unexpected Member discovered in field: %s", lf.Name)
	default:
		panic(fmt.Sprintf("Unknown field type %T", fVal.Value))
	}

	return rawVal, nil
}

func (lds *LLRPDefinitionStore) encodeField(fieldVals *list.List, lf *LLRPField) (bl *byteList, err error) {

	bl = newByteList()
	fmt.Println("Encoding Field:", lf.Name)

	// Now, depending on the type, we need to either shift
	// the data and add it as a u8, or add it as a
	// fieldwidth specific value
	switch lf.Type {
	case "u1":
		fVal := fieldVals.Front()
		rawVal, err := lf.getFieldValue(fVal)
		if err != nil {
			return nil, err
		}
		v := uint8((uint8(rawVal) & 0x1) << 7)
		bl.addU8(v)
	case "u1v":
		vLen := fieldVals.Len()
		bl.addU16(uint16(vLen))
		for fVal := fieldVals.Front(); fVal != nil; fVal = fVal.Next() {
			rawVal, err := lf.getFieldValue(fVal)
			if err != nil {
				return nil, err
			}
			bl.addU8((uint8(rawVal)))
		}
	case "u2":
		fVal := fieldVals.Front()
		rawVal, err := lf.getFieldValue(fVal)
		if err != nil {
			return nil, err
		}
		v := uint8((uint8(rawVal) & 0x3) << 6)
		bl.addU8(v)
	case "u8":
		fVal := fieldVals.Front()
		rawVal, err := lf.getFieldValue(fVal)
		if err != nil {
			return nil, err
		}
		bl.addU8(uint8(rawVal))
	case "u8v":
		vLen := fieldVals.Len()
		bl.addU16(uint16(vLen))
		for fVal := fieldVals.Front(); fVal != nil; fVal = fVal.Next() {
			rawVal, err := lf.getFieldValue(fVal)
			if err != nil {
				return nil, err
			}
			bl.addU8((uint8(rawVal)))
		}
	case "utf8v":
		//TODO
	case "u16":
		fVal := fieldVals.Front()
		rawVal, err := lf.getFieldValue(fVal)
		if err != nil {
			return nil, err
		}
		bl.addU16(uint16(rawVal))
	case "u16v":
		vLen := fieldVals.Len()
		bl.addU16(uint16(vLen))
		for fVal := fieldVals.Front(); fVal != nil; fVal = fVal.Next() {
			rawVal, err := lf.getFieldValue(fVal)
			if err != nil {
				return nil, err
			}
			bl.addU16((uint16(rawVal)))
		}
	case "u32":
		fVal := fieldVals.Front()
		rawVal, err := lf.getFieldValue(fVal)
		if err != nil {
			return nil, err
		}
		bl.addU32(uint32(rawVal))
	case "u32v":
		vLen := fieldVals.Len()
		bl.addU16(uint16(vLen))
		for fVal := fieldVals.Front(); fVal != nil; fVal = fVal.Next() {
			rawVal, err := lf.getFieldValue(fVal)
			if err != nil {
				return nil, err
			}
			bl.addU32((uint32(rawVal)))
		}
	case "u64":
		//TODO
	case "u96":
		// TODO
	}

	return bl, nil
}

func (lds *LLRPDefinitionStore) encodeTLVParam(mm *MsgMember, lpd *LLRPParameterDef,
	strict bool) (bl *byteList, err error) {
	bl = newByteList()

	fmt.Println("Encoding TLV Parameter: ", lpd.Name, mm.Key)
	mm.print(0)
	// Add the typenum field
	bl.addU16(uint16(lpd.TypeNum))

	// For TLV Parameters, we'll be encoding the length of the
	// subsequent data payload as well as the type.
	paramLen := uint16(4)
	payloadBytes := newByteList()
	var tBytes *byteList
	for member := lpd.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			lp := member.Value.(*LLRPParameter)
			mmParam, ok := mm.findSubMember(lp.Type.Name)
			//mmParam, ok := mm.Values[lp.Type.Name]
			
			if !ok  && strict  && lp.RepeatMin > 0 {
				// We didn't find the parameter, we're
				// in strict mode, and it is required
				return nil, fmt.Errorf("TLVParameter %s is missing Parameter: %s",
					lpd.Name, lp.Type.Name)
			} else if !ok && !strict  && lp.RepeatMin > 0 {
				// We didn't find the parameter, we're
				// not in strict mode, but it IS required
				fmt.Printf("TLVParameter %s is missing Parameter: %s...Autofilling\n",
					lpd.Name, lp.Type.Name)

				tBytes = lp.getDefaultByteList()
				payloadBytes.extend(tBytes)
			} else if ok {
				// We found the parameter, encode it
				fmt.Println("here", lp.Type.Name, lp.RepeatMin, mmParam.Key)
				lpDef := lds.ParamNameMap[lp.Type.Name]
				
				var tBytes *byteList
				if lpDef.Encoding == TV {
					tBytes, err = lds.encodeTVParam(mmParam, lpDef, strict)
				} else {
					tBytes, err = lds.encodeTLVParam(mmParam, lpDef, strict)
				}
				if err != nil {
					return nil, err
				}
				
				payloadBytes.extend(tBytes)
				fmt.Println("also here")
				// Important so that we skip the next field
				member = member.Next()
			}
		case *LLRPField:
			field := member.Value.(*LLRPField)
			// Now find the field we want to encode
			mmField, ok := mm.getFieldList(field.Name)
			fmt.Println("Trying field:", field.Name, ok)

			// Make sure it exists
			if !ok  && strict {
				return nil, fmt.Errorf("TLVParameter %s is missing field: %s",
					lpd.Name, field.Name)
			} else if !ok && !strict {
				fmt.Printf("TLVParameter %s is missing field: %s...Autofilling\n",
					lpd.Name, field.Name)
				zList := list.New()
				zList.PushBack(float64(0))
				tBytes, err = lds.encodeField(zList, field)
				if err != nil {
					return nil, err
				}
			} else if ok{
				tBytes, err = lds.encodeField(mmField, field)
				if err != nil {
					return nil, err
				}
			}
			payloadBytes.extend(tBytes)
		case *LLRPReserved:
			// This is just a pass
		case *LLRPChoice:
			fmt.Println("Its a choice!")
			choice := member.Value.(*LLRPChoice)
			// Now, walk through each possible parameter
			// choice and check ot see if it appears in
			// the message heirarchy

			choicesFilled := 0
			for k,_ := range choice.Type.ChoiceMap {
				// Now find the field we want to encode
				mmChoice, ok := mm.findSubMember(k)
				fmt.Println("Trying:", k, ok)
				// TODO: handle that nested choice bullshit

				// Make sure it exists
				if ok && choicesFilled > choice.RepeatMax {
					return nil, fmt.Errorf("TLVParam %s has too many choices!", lpd.Name)
				} else if ok {
					choicesFilled++
					lpDef := lds.ParamNameMap[k]
					
					var tBytes *byteList
					if lpDef.Encoding == TV {
						tBytes, err = lds.encodeTVParam(mmChoice, lpDef, strict)
					} else {
						tBytes, err = lds.encodeTLVParam(mmChoice, lpDef, strict)
					}
					fmt.Println("Just encoded tv/tlv param in choice")
					if err != nil {
						return nil, err
					}
					payloadBytes.extend(tBytes)
				}
			}

			if choicesFilled < choice.RepeatMin {
				return nil, fmt.Errorf("TLVParam %s is missing Choice: %s...Unable to autofill",
						lpd.Name, choice.Type.Name)
			}
			
		}
	}

	paramLen += uint16(payloadBytes.Len())
	bl.addU16(paramLen)
	bl.extend(payloadBytes)

	return bl, nil
}



func (lds *LLRPDefinitionStore) encodeTVParam(mm *MsgMember,lpd *LLRPParameterDef,
	strict bool) (bl *byteList, err error) {


	bl = newByteList()

	fmt.Println("Encoding TV Parameter: ", lpd.Name)
	// Add the typenum field
	pType := uint8(1<<7 | uint8(lpd.TypeNum))
	bl.addU8(pType)

	// For TV parameters, we're just encoding fields
	for member := lpd.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			return nil, fmt.Errorf("Unexpected Parameter member in TV Parameter: %s", lpd.Name)
		case *LLRPField:
			field := member.Value.(*LLRPField)
			// Now find the field we want to encode
			mmField, ok := mm.findSubMember(field.Name)

			var tBytes *byteList
			// Make sure it exists
			if !ok  && strict {
				return nil, fmt.Errorf("Parameter %s is missing field: %s",
					lpd.Name, field.Name)
			} else if !ok && !strict {
				zList := list.New()
				zList.PushBack(float64(0))
				tBytes, err = lds.encodeField(zList, field)
				if err != nil {
					return nil, err
				}
			} else {
				tBytes, err = lds.encodeField(&mmField.Values, field)
				if err != nil {
					return nil, err
				}
			}

			bl.extend(tBytes)
		case *LLRPReserved:
			// This is just a pass
		case *LLRPChoice:
			return nil, fmt.Errorf("Unexpected Choice member in TV Parameter: %s", lpd.Name)
		}
	}


	return bl, nil
}

func (lf *LLRPField) getDefaultByteList() (bl *byteList) {
	bl = newByteList()
	fmt.Println("Autofilling Field:", lf.Name)
	switch lf.Type {
	case "u1", "u1v", "u2", "u8", "u8v":
		bl.addU8(0)
	case "u16", "u16v":
		bl.addU16(0)
	case "u32", "u32v":
		bl.addU32(0)
	}

	return bl
}

func (lc *LLRPChoice) getDefaultByteList() (bl *byteList) {
	bl = newByteList()

	// Make sure we even need this
	if lc.RepeatMin < 1 {
		return bl
	}

	for k,v := range lc.Type.ChoiceMap {
		fmt.Println(k, v)
	}
	return bl
}

func (lp *LLRPParameter) getDefaultByteList() (bl *byteList) {
	bl = newByteList()

	if lp.RepeatMin < 1 {
		fmt.Printf("Non-Required Parameter: %s, skipping\n", lp.Type.Name)
		return bl
	} else {
		fmt.Println("Autofilling Parameter:", lp.Type.Name)
	}

	var paramLen uint16
	if lp.Type.Encoding == TV {
		// Add the typenum field
		pType := uint8(1<<7 | uint8(lp.Type.TypeNum))
		bl.addU8(pType)
	} else {
		// Add the typenum field
		bl.addU16(uint16(lp.Type.TypeNum))
		paramLen = uint16(4)
	}

	var tBytes *byteList
	payloadBytes := newByteList()
	for member := lp.Type.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			param := member.Value.(*LLRPParameter)
			// make sure we avoid infinite recursion
			if param.Type.Name == lp.Type.Name {
				continue
			} else {
				tBytes = param.getDefaultByteList()
				payloadBytes.extend(tBytes)
			}
		case *LLRPField:
			field := member.Value.(*LLRPField)
			tBytes = field.getDefaultByteList()
			payloadBytes.extend(tBytes)
		case *LLRPReserved:
			// this is a pass
		case *LLRPChoice:
			choice := member.Value.(*LLRPChoice)
			tBytes = choice.getDefaultByteList()
			payloadBytes.extend(tBytes)
		}
	}

	paramLen += uint16(payloadBytes.Len())
	bl.addU16(paramLen)
	bl.extend(payloadBytes)
	return bl
}

// TODO: Rename this to something that makes more sense
func (lds *LLRPDefinitionStore) walkParam(mm *MsgMember, lp *LLRPParameter,
	parentName string, strict bool) (bl *byteList, err error) {

	fmt.Printf("Walking %s\n", parentName)
	bl = newByteList()
	mm.print(0)
	mmVals, ok := mm.getSubMemberList(lp.Type.Name)
	if !ok {
		// We need to make sure that this is a
		// required parameter/field  If it is,
		// we need to throw an error,
		// otherwise just move onto the next
		// member
		if lp.RepeatMin > 0 && strict {
			return nil, fmt.Errorf("%s is missing parameter: %s\n",
				parentName, lp.Type.Name)
		} else if lp.RepeatMin > 0 && !strict{
			fmt.Printf("%s is missing parameter: %s...Autofilling\n",
				parentName, lp.Type.Name)

			tBytes := lp.getDefaultByteList()
			bl.extend(tBytes)
			return bl, nil
		} else {
			fmt.Println("Returning, nothing to encode")
			// RepeatMin must be zero, just return an
			// empty list
			return bl, nil
		}
	}

	// Now make sure that the number of Values
	// doesn't exceed the parameter repeat max
	if mm.getSubMemberCount(lp.Type.Name) > lp.RepeatMax {
		return nil, fmt.Errorf("%s contains too many of parameter: %s",
			parentName, lp.Type.Name)
	}

	fmt.Printf("%s has correct parameter counts\n", parentName)
	// Ok, if we're here, that means we have the specified number
	// of this parameter and should begin encoding them one by one
	// extending our return byte list for each of them
	retBytes := newByteList()
	var tBytes *byteList
	for p := mmVals.Front(); p != nil; p = p.Next() {
		mmParm := p.Value.(*MsgMember)
		if lp.Type.Encoding == TV {
			tBytes, err = lds.encodeTVParam(mmParm, lp.Type, strict)
		} else {
			tBytes, err = lds.encodeTLVParam(mmParm, lp.Type, strict)
		}

		if err != nil {
			return nil, err
		}
		retBytes.extend(tBytes)
	}
	return retBytes, nil
}


// TODO: handle unknown fields in the message member
func (lds *LLRPDefinitionStore) encodeMsg(mm *MsgMember, msg_id uint32, strict bool) (*[]byte, error) {
	bl := newByteList()

	// Pull the message out of the message name map in the
	// definition store
	msgDef, ok := lds.MessageNameMap[mm.Key]
	if !ok {
		return nil, fmt.Errorf("Invalid Message: %s", mm.Key)
	}
	fmt.Println("Encoding Message: ", msgDef.Name)

	// RSVD + Ver + Message Type
	mType := uint16((0x1 << 10) | msgDef.TypeNum)
	//fmt.Println(mType)
	bl.addU16(mType)

	// This bytelist will contain all of the bytes from the
	// parameters we contain
	payloadBytes := newByteList()

	// Now we walk through each of the members (fields/parameters)
	// in the message definition and encode them accordingly
	// (Note that there will likely be some recursion involved)
	var msgLen uint32
	var retBytes *byteList
	var err error
	for member := msgDef.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			lp := member.Value.(*LLRPParameter)
			retBytes, err = lds.walkParam(mm, lp, msgDef.Name, strict)
			if err != nil {
				return nil, err
			}
			payloadBytes.extend(retBytes)
		case *LLRPField:
			lf := member.Value.(*LLRPField)
			// Now find the field we want to encode
			mmField, _ := mm.findSubMember(lf.Name)
			tBytes, err := lds.encodeField(&mmField.Values, lf)
			if err != nil {
				return nil, err
			}
			payloadBytes.extend(tBytes)
		}
	}

	msgLen = uint32(payloadBytes.Len() + 10)
	bl.addU32(msgLen)
	bl.addU32(msg_id)
	bl.extend(payloadBytes)

	return bl.getBytes(), nil
}
