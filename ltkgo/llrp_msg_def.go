package ltkgo

import (
	"container/list"
	"sync"
	"fmt"
)
// TODO: Refactor to make a lot of these members private...

// Encoding type of paremeter
const (
	TV = iota
	TLV
)

type LLRPDefinitionStore struct {
	sync.RWMutex
	MessageNameMap map[string]*LLRPMessageDef
	ParamNameMap   map[string]*LLRPParameterDef
	EnumNameMap    map[string]*LLRPEnumDef
	ChoiceNameMap  map[string]*LLRPChoiceDef
	MessageIntMap  map[int]*LLRPMessageDef
	ParamIntMap    map[int]*LLRPParameterDef
}

type LLRPMessageDef struct {
	Name         string
	TypeNum      int
	Required     bool
	ResponseTypeName string
	Members      list.List
}

type LLRPField struct {
	Type  string
	Name  string
	Enumeration *LLRPEnumDef
}

type LLRPReserved struct {
	BitCount int
}

type LLRPParameterDef struct {
	Name     string
	TypeNum  int
	Required bool
	Members  list.List
	Encoding int
}

type LLRPEnumDef struct {
	Name string
	ValMap map[string]int
}

type LLRPChoice struct {
	RepeatMin int
	RepeatMax int
	Type *LLRPChoiceDef
}

// This seems silly, but then again...so is LLRP.  Sometimes, the
// parameters contained inside of a choice aren't parameters at
// all...they're more choices.  Turtles all the way down
type LLRPChoiceMember struct {
	Param  *LLRPParameterDef
	Choice *LLRPChoiceDef
}

type LLRPParameter struct {
	RepeatMin int
	RepeatMax int
	Type      *LLRPParameterDef
}

type LLRPChoiceDef struct {
	Name string
	ChoiceMap map[string]*LLRPChoiceMember
}

func indent(levels int) {
	for i := 0; i < levels; i++ {
		fmt.Printf("    ")
	}
}

func (lr *LLRPReserved) Print(indentLvl int) {
	indent(indentLvl)
	fmt.Printf("Reserved %d bits\n", lr.BitCount)
}

func (lc *LLRPChoice) Print(indentLvl int) {
	indent(indentLvl)
	fmt.Printf("Choice: %s\n", lc.Type.Name)
}

func (lp *LLRPParameter) Print(indentLvl int) {
	indent(indentLvl)
	fmt.Printf("Parameter: %s\n", lp.Type.Name)

	for member := lp.Type.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			param := member.Value.(*LLRPParameter)
			if param.Type.Name == lp.Type.Name {
				indent(indentLvl + 1)
				fmt.Printf("Parameter: %s <INFINITE LOOP!>\n", param.Type.Name)
				return
			}
			param.Print(indentLvl + 1)
		case *LLRPField:
			field := member.Value.(*LLRPField)
			field.Print(indentLvl + 1)
		case *LLRPReserved:
			res := member.Value.(*LLRPReserved)
			res.Print(indentLvl + 1)
		case *LLRPChoice:
			choice := member.Value.(*LLRPChoice)
			choice.Print(indentLvl + 1)
		}
	}
}

func (lf *LLRPField) Print(indentLvl int) {
	indent(indentLvl)
	fmt.Printf("Field: %s\n", lf.Name)
}

func(lmd *LLRPMessageDef) Print(indentLvl int) {
	fmt.Printf("Message[%4d]: %s\n", lmd.TypeNum, lmd.Name)

	if lmd.Members.Len() == 0 {
		return
	}

	for member := lmd.Members.Front(); member != nil; member = member.Next() {
		switch member.Value.(type) {
		case *LLRPParameter:
			lp := member.Value.(*LLRPParameter)
			lp.Print(indentLvl + 1)
		case *LLRPField:
			lf := member.Value.(*LLRPField)
			lf.Print(indentLvl + 1)
		}
	}
}

func (lds *LLRPDefinitionStore) DumpHeirarchy() {
	fmt.Println("LLRP Definition Store Heirarchy")
	fmt.Println("-------------------------------")
	for msg := range(lds.MessageNameMap) {
		lds.MessageNameMap[msg].Print(0)
	}
}

func NewLLRPDefinitionStore() *LLRPDefinitionStore {
	ds := LLRPDefinitionStore {
		MessageNameMap: make(map[string]*LLRPMessageDef),
		ParamNameMap:   make(map[string]*LLRPParameterDef),
		MessageIntMap:  make(map[int]*LLRPMessageDef),
		ParamIntMap:    make(map[int]*LLRPParameterDef),
		EnumNameMap:    make(map[string]*LLRPEnumDef),
		ChoiceNameMap:  make(map[string]*LLRPChoiceDef),

	}
	return &ds
}
