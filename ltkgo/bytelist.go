package ltkgo

import (
	"bytes"
	"errors"
)

type byteList struct {
	bback []byte
	head int
	tail int
}

func (bl *byteList) addU32(u uint32) {
	if bl.tail == len(bl.bback) - 1 {
		// Extend to ensure we can fit more incoming data
		bl.bback = append(bl.bback, make([]byte, 256)...)
	}
	bl.bback[bl.tail] = uint8((u & 0xff000000) >> 24)
	bl.bback[bl.tail + 1] = uint8((u & 0xff0000) >> 16)
	bl.bback[bl.tail + 2] = uint8((u & 0xff00) >> 8)
	bl.bback[bl.tail + 3] = uint8(u & 0xff)
	bl.tail += 4
}

func (bl *byteList) getU32() (u uint32, err error) {
	if bl.tail - bl.head < 4 {
		return 0, errors.New("Unable to get u32, not enough bytes!")
	}
	u1 := bl.bback[bl.head]
	u2 := bl.bback[bl.head + 1]
	u3 := bl.bback[bl.head + 2]
	u4 := bl.bback[bl.head + 3]
	u = uint32((uint32(u1) << 24) | (uint32(u2) << 16) | (uint32(u3) << 8) | uint32(u4))
	bl.head += 4
	return u, nil
}

func (bl *byteList) getU64() (u uint64, err error) {
	if bl.tail - bl.head < 8 {
		return 0, errors.New("Unable to get u64, not enough bytes!")
	}
	u1 := bl.bback[bl.head]
	u2 := bl.bback[bl.head + 1]
	u3 := bl.bback[bl.head + 2]
	u4 := bl.bback[bl.head + 3]
	u5 := bl.bback[bl.head + 4]
	u6 := bl.bback[bl.head + 5]
	u7 := bl.bback[bl.head + 6]
	u8 := bl.bback[bl.head + 7]
	u = uint64((uint64(u1) << 56) | (uint64(u2) << 48) | (uint64(u3) << 40) | (uint64(u4) << 32) |
		(uint64(u5) << 24) | (uint64(u6) << 16) | (uint64(u7) << 8) | uint64(u8))
	bl.head += 8
	return u, nil
}

func (bl *byteList) peekU32() (u uint32, err error) {
	if bl.tail - bl.head < 4 {
		return 0, errors.New("Unable to get u32, not enough bytes!")
	}

	u1 := bl.bback[bl.head]
	u2 := bl.bback[bl.head + 1]
	u3 := bl.bback[bl.head + 2]
	u4 := bl.bback[bl.head + 3]
	u = uint32((uint32(u1) << 24) | (uint32(u2) << 16) | (uint32(u3) << 8) | uint32(u4))
	return u, nil
}

func (bl *byteList) addU16(u uint16) {
	if bl.tail == len(bl.bback) - 1 {
		// Extend to ensure we can fit more incoming data
		bl.bback = append(bl.bback, make([]byte, 256)...)
	}
	bl.bback[bl.tail] = uint8(u >> 8)
	bl.bback[bl.tail + 1] = uint8(u & 0xff)
	bl.tail += 2
}

func (bl *byteList) getU16() (u uint16, err error) {
	if bl.tail - bl.head < 2 {
		return 0, errors.New("Unable to get u16, not enough bytes!")
	}
	// Faster than creating a reader and doing a binary.read()
	u1 := bl.bback[bl.head]
	u2 := bl.bback[bl.head + 1]
	u = uint16((uint16(u1) << 8) | uint16(u2))
	bl.head += 2
	return u, nil
}

func (bl *byteList) peekU16() (u uint16, err error) {
	if bl.tail - bl.head < 2 {
		return 0, errors.New("Unable to get u16, not enough bytes!")
	}
	//u1 := bl.bback[bl.head]
	//u2 := bl.bback[bl.head + 1]
	u = uint16((uint16(bl.bback[bl.head]) << 8) | uint16(bl.bback[bl.head+1]))
	return u, nil
}

func (bl *byteList) addU8(u uint8) {
	if bl.tail == len(bl.bback) - 1 {
		// Extend to ensure we can fit more incoming data
		bl.bback = append(bl.bback, make([]byte, 256)...)
	}
	bl.bback[bl.tail] = u
	bl.tail++
}

func (bl *byteList) getU8() (u uint8, err error) {
	if len(bl.bback) - bl.head < 1 {
	 	return 0, errors.New("Unable to get u8, not enough bytes!")
	}
	u = bl.bback[bl.head]
	bl.head++
	return u, nil
}

func (bl *byteList) peekU8() (u uint8, err error) {
	if len(bl.bback) - bl.head < 1 {
	 	return 0, errors.New("Unable to get u8, not enough bytes!")
	}
	u = bl.bback[bl.head]
	return u, nil
}

func (bl *byteList) getByteSlice(l int) (*[]byte, error) {
	if bl.Len() < l {
		return nil, errors.New("Not enough bytes to return requested slice")
	}

	r := bl.bback[bl.head: bl.head + l]
	bl.head += l

	return &r, nil
}

func (bl *byteList) extend(extBl *byteList) {
	if len(bl.bback) - bl.tail < extBl.Len() {
		// Extend to ensure we can fit more incoming data
		bl.bback = append(bl.bback, make([]byte, extBl.Len())...)
	}
	
	copy(bl.bback[bl.tail:], extBl.bback[extBl.head:extBl.tail])
	bl.tail += extBl.Len()
	//bl.bback = append(bl.bback, extBl.bback[extBl.head:extBl.tail]...)
}

func (bl *byteList) Len() int {
	return bl.tail - bl.head
}

func (bl *byteList) getBytes() *[]uint8 {
	retBytes := make([]byte, bl.tail - bl.head)
	copy(retBytes, bl.bback[bl.head:bl.tail])
	return &retBytes
}

func newByteListFromBuf(b *bytes.Buffer) *byteList {
	bl := new(byteList)
	bl.bback = make([]byte, b.Len())
	copy(bl.bback, b.Bytes())
	bl.tail = b.Len()
	return bl
}

func newByteListFromBytes(d *[]byte) *byteList {
	bl := new(byteList)
	bl.bback = make([]byte, len(*d))
	copy(bl.bback, *d)
	bl.tail = len(*d)
	return bl
}

func newByteList() *byteList {
	bl := new(byteList)
	bl.bback = make([]byte, 1024)
	bl.head = 0
	bl.tail = 0
	return bl
}
