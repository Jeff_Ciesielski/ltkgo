package ltkgo

import (
	"container/list"
	"fmt"
)

type MsgMember struct {
	Key string
	Values list.List
}

func (mm *MsgMember) AddValue(val interface{}) {

	switch val.(type) {
	case float64, string, *MsgMember, *[]byte, *[]float64:
		mm.Values.PushBack(val)
	default:
		panic("Unknown field type")
	}
	
}

func (mm *MsgMember) getFieldList(reqKey string) (*list.List, bool) {
	rList := new(list.List)
	ok := false
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case float64, string:
			rList.PushBack(val)
			ok = true
		case *MsgMember:
			fmm := val.Value.(*MsgMember)
			if fmm.Key == reqKey {
				for fval := fmm.Values.Front(); fval != nil; fval = fval.Next() {
					switch fval.Value.(type) {
					case float64, string:
						rList.PushBack(fval.Value)
						ok = true
					}
				}
			}
		}
	}
	return rList, ok
}

func (mm *MsgMember) getFieldCount(reqKey string) (count int) {
	count = 0
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case float64, string:
			count++
		}
	}
	return count
}

func (mm *MsgMember) getSubMemberCount(reqKey string) (count int) {
	count = 0
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case *MsgMember:
			mem := val.Value.(*MsgMember)
			if mem.Key == reqKey {
				count++
			}
		}
	}
	return count
}

func (mm *MsgMember) getSubMemberList(reqKey string) (*list.List,bool) {
	rList := new(list.List)
	ok := false
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case *MsgMember:
			mem := val.Value.(*MsgMember)
			if mem.Key == reqKey {
				rList.PushBack(mem)
				ok = true
			}
		}
	}
	return rList, ok
	
}

func (mm *MsgMember) findSubMember(reqKey string) (*MsgMember, bool) {
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case *MsgMember:
			mem := val.Value.(*MsgMember)
			if mem.Key == reqKey {
				return mem, true
			}
		}
	}
	
	return nil, false
}

func (mm *MsgMember) print(indentLvl int) {
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case float64:
			indent(indentLvl + 1)
			fmt.Printf("MF-%s\n",mm.Key)
			indent(indentLvl + 2)
			fmt.Println("-Float64:", val.Value.(float64))
		case string:
			indent(indentLvl + 1)
			fmt.Printf("MF-%s\n",mm.Key)
			indent(indentLvl + 2)
			fmt.Println("-String:", val.Value.(string))
		case *MsgMember:
			indent(indentLvl)
			fmt.Printf("MM-%s\n",mm.Key)
			msg := val.Value.(*MsgMember)
			msg.print(indentLvl + 1)
		default:
			fmt.Printf("Unknown field type %T\n", val.Value)
		}
	}
}

func (mm *MsgMember) Dump() {
	fmt.Println("\nMessage Member Heirarchy Dump")
	fmt.Println("-----------------------------")
	mm.print(0)
}


func NewMsgMember() *MsgMember {
	mm := new(MsgMember)
	return mm
}
