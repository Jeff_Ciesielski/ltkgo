package ltkgo

import (
	"encoding/json"
	"bytes"
	"fmt"
	"strings"
)

func (mm *MsgMember) unrollJsonInterface (iface interface{}) error {

	m := iface.(map[string]interface{})
	for k, v := range m {
		msgMem := MsgMember{Key: k}
		switch vv := v.(type) {
		case string, float64:
			msgMem.AddValue(vv)
			mm.AddValue(&msgMem)
		case []interface{}:
			for val := range vv {
				msgMem.AddValue(val)
			}
			mm.AddValue(&msgMem)
		case interface{}:
			msgMem.Key = k
			msgMem.unrollJsonInterface(v)
			mm.AddValue(&msgMem)

		default:
			fmt.Println(k, "is of a type I don't know how to handle")
		}
	}
	return nil
}

func (mm *MsgMember) toJson(top bool) (*[]byte, error) {

	buffer := new(bytes.Buffer)
	//fmt.Println("Encoding:", mm.Key)

	if top {
		buffer.WriteString("{")
	}

	tVal := mm.Values.Front()

	_, isParam := tVal.Value.(*MsgMember)

	if isParam {
		buffer.WriteString(fmt.Sprintf("\"%s\":", mm.Key))
		buffer.WriteString("{")
	}

	firstP := true
	for val := mm.Values.Front(); val != nil; val = val.Next() {
		switch val.Value.(type) {
		case float64:
			if !firstP {
				buffer.WriteString(",")
			}
			firstP = false
			buffer.WriteString(fmt.Sprintf("\"%v\":%v", mm.Key, val.Value.(float64)))
		case string:
			if !firstP {
				buffer.WriteString(",")
			}
			firstP = false
			buffer.WriteString(fmt.Sprintf("\"%v\":\"%v\"", mm.Key, val.Value.(string)))
		case *MsgMember:
			if !firstP {
				buffer.WriteString(",")
			}
			firstP = false
			mmChild := val.Value.(*MsgMember)
			rBytes, err := mmChild.toJson(false)
			//_, err := mmChild.toJson(false)
			if err != nil {
				return nil, err
			}
			buffer.Write(*rBytes)
		case *[]byte:
			if !firstP {
				buffer.WriteString(",")
			}
			firstP = false
			buffer.WriteString(fmt.Sprintf("\"%v\":", mm.Key,))
			v := *val.Value.(*[]byte)
			buffer.WriteString(strings.Replace(fmt.Sprintf("%v", v), " ", ",", -1))
		case *[]float64:
			// TODO
		}
	}

	if isParam {
		buffer.WriteString("}")
	}

	if top {
		buffer.WriteString("}")
		rBytes := buffer.Bytes()
		return &rBytes, nil
	} else {
		rBytes := buffer.Bytes()
		return &rBytes, nil
	}


}

func MsgMemberFromJson (jsn []byte) (*MsgMember, error) {

	var f interface{}

	err := json.Unmarshal(jsn, &f)
	if err != nil {
		return nil, err
	}


	msgMem := NewMsgMember()

	m := f.(map[string]interface{})
	// TODO: Check length before we start
	for k, v := range m {
		switch v.(type) {
		case interface{}:
			msgMem.Key = k
			msgMem. unrollJsonInterface(v)

		default:
			fmt.Println(k, "is of a type I don't know how to handle")
		}
	}

	return msgMem, nil
}
