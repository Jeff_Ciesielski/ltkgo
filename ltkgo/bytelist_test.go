package ltkgo

import (
	"testing"
)

func TestByteListLen(t *testing.T) {
	bl := newByteList()

	bl.addU8(uint8(1))
	bl.addU16(uint16(32))
	bl.addU32(uint32(0xffffffff))

	if bl.Len() != 7 {
		t.Error("Incorrect length")
	}
}

func TestExtendBytelist(t *testing.T) {
	bl := newByteList()

	bl.addU8(uint8(1))
	bl.addU16(uint16(32))
	bl.addU32(uint32(0xffffffff))

	bl2 := newByteList()

	bl2.addU8(uint8(1))
	bl2.addU16(uint16(32))
	bl2.addU32(uint32(0xffffffff))

	bl.extend(bl2)

	if bl.Len() != 14 {
		t.Error("Unable to extend bytelist")
	}

}
